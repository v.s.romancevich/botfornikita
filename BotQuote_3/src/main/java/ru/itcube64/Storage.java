package ru.itcube64;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class Storage {
    private ArrayList<String> quoteList;
    private ArrayList<String> jokeList;
    private ArrayList<String> weatherList;


    public Storage() {
        quoteList = new ArrayList<>();
        jokeList = new ArrayList<>();
        weatherList = new ArrayList<>();
        parserQuote("https://citatnica.ru/citaty/mudrye-tsitaty-velikih-lyudej");
        parserJoke("https://anekdoty.ru/pro-programmistov/?ysclid=lq7rgsm8kn5733098");
        parserWeather("https://yandex.ru/pogoda/volsk?ysclid=lq7t0sw6es636175570&lat=52.047012&lon=47.389446");

//        quoteList.add("Начинать всегда стоит с того, что сеет сомнения. \n\nБорис Стругацкий");
//        quoteList.add("80% успеха - это появится в нужном месте в нужное время. \n\nВуди Аллен");
//        quoteList.add("Мы должны признать очевидное: понимают лишь те, кто хочет понять. \n\nБернар Вербер");
    }


    public String getRandQuote() {
        int randValue = (int) (Math.random() * quoteList.size());

        return quoteList.get(randValue);
    }

    public String getRandJoke() {
        int randValue = (int) (Math.random() * jokeList.size());

        return jokeList.get(randValue);
    }

    public String getRandWeather() {
        int randValue = (int) (Math.random() * weatherList.size());

        return weatherList.get(randValue);
    }

    private void parserQuote(String strURL) {
        String className = "su-note-inner su-u-clearfix su-u-trim";
        Document document = null;

        try {
            document = Jsoup.connect(strURL).maxBodySize(0).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements elQuote = document.getElementsByClass(className);
        elQuote.forEach(el -> {
            quoteList.add(el.text());
        });
    }

    private void parserJoke(String strURL) {
        String className = "holder-body";
        Document document = null;

        try {
            document = Jsoup.connect(strURL).maxBodySize(0).get();
        } catch (IOException et) {
            et.printStackTrace();
        }

        Elements elJoke = document.getElementsByClass(className);
        elJoke.forEach(el -> {
            jokeList.add(el.text());
        });
    }

    private void parserWeather(String strURL) {
        String className = "title-icon__text";
        Document document = null;

        try {
            document = Jsoup.connect(strURL).maxBodySize(0).get();
        } catch (IOException ed) {
            ed.printStackTrace();
        }

        Elements elWeather = document.getElementsByClass(className);
        elWeather.forEach(el -> {
            weatherList.add(el.text());
        });
    }
}
